#ifndef Gnuplot2Show_H
#define Gnuplot2Show_H

#include <iostream>
#include <fstream>
using namespace std;

void Gnuplot2Show(double *x, double *y, int num, float Ex){

    //写入 温度--时间 关系
    ofstream out("Water.dat", ios::trunc);
    if(out.is_open()) {
        for(int i=0; i<num; i++)
            out << x[i] << ' '<< y[i] << endl;
        out<<endl << x[0]<<' '<<Ex << endl;
        out<< x[num-1]<<' '<<Ex << endl;
    }
    out.close();

    //调用 gnuplot 完成数据可视化 
    char gnuplotPath[] = "gnuplot.exe";
    FILE* gp = _popen(gnuplotPath,"w");
    if (gp == NULL)
    {
        cout<<("Cannotopen gnuplot!\n")<<endl;
    }
    else {
        fprintf(gp,"plot \"Water.dat\" with lines lc rgb 'blue'\n");
        fprintf(gp,"pause mouse\n");
        _pclose(gp);
    }
}

#endif