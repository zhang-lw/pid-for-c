#ifndef PID_H
#define PID_H

//pid基本数据结构
struct pidStruct
{
    float kp;       //比例系数
    float ki;       //积分系数
    float kd;       //微分系数
    float k;        //pid整体系数（控制pid对被控系统的影响程度）
};

/*****************************************
*               pid类                    *
******************************************/
class Pid
{
    private:
        double ER = 0;            //存储误差的累计
        double lastEr = 0;        //记录上一次误差

    public:
        float EX = 0;             //存储目标系统值
        struct pidStruct pid;     //初始化调用pid系数结构

        //设置pid系数
        void setPid(float p, float i, float d, float k = 0.1){
            pid.k = k;
            pid.kp = p;
            pid.ki = i;
            pid.kd = d;
        }

        //重置内部值，一般用于一个pid控制多个系统使用
        void reSet(){
            ER = 0;
            lastEr = 0;
            EX = 0;
        }

        //设置期望值
        void setEx(float Ex){
            EX = Ex;
        }

        //运算，仅算一个点，配合循化使用
        double clac(float inf){         //inf是系统反馈值，属性应当与期望值相同
            float e = EX - inf;
            double response = pid.k*(pid.kp*e + pid.ki*ER +  pid.kd*(e-lastEr));
            ER += e;
            lastEr = e;
            return response;
        }
};


#endif // PID_H
