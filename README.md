# PID for C/C++

#### 介绍

用于 C/C++ 学习最基本的[PID](https://baike.baidu.com/item/PID%E7%AE%97%E6%B3%95/4660106?fr=aladdin)算法。该项目，在本质上实现PID算法，小巧简便，修改方便，适用于大多数带有反馈系统控制场合。

#### 使用场合
1.带有反馈的系统系统控制</br>
2.硬件方面可用于 [Arduino](https://www.arduino.cc)、[stm32](https://www.stmcu.com.cn/)等支持C/C++的硬件设备</br>
3.轻量化的 C/C++ 数据分析处理

#### 使用说明

核心代码：  **PID.h**

    git clone https://gitee.com/zhang-lw/pid-for-c.git


Demo文件
    
    Dome
        dome.cpp
        Gnuplot2Show.h
        make.sh
        make.bat

`demo.cpp` 主程序</br>
`Gnuplot2Show.h` 是基于[Gnuplot](http://www.gnuplot.info/)实现的可视库，对于[Gnuplot](http://www.gnuplot.info/)可自行百度</br>
`make.sh` shell运行主程序， `make.bat` windows运行主程序。也可自行运行学习调试运行。</br>
`由于程序实现基于Gnuplot实现可视化，所以在运行前Gnuplot设置环境变量才可正常运行`

该PID控制水加热到80摄氏度，效果：</br>
![warter.png](Demo/src/water.png)

#### 参与贡献

1.  zhang-lw

